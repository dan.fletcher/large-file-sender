' create an instance of the File Browser
Set ObjFSO = CreateObject("UserAccounts.CommonDialog")

'setup the File Browser specifics
ObjFSO.Filter = "Windows Zip Files|*.zip|Windows Zip Files"
ObjFSO.FilterIndex = 3
ObjFSO.InitialDir = "c:\"

' show the file browser and return the selection (or lack of) to InitFSO
InitFSO = ObjFSO.ShowOpen

If InitFSO = False Then
    Wscript.Echo "Script Error: Please select a file!"
    Wscript.Quit
Else

' file name has been selected

' select random file name

     Randomize
     sCharacters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVW1234567890"
     For intI = 1 To 64
          sRandom = sRandom + Mid(sCharacters, Int(Rnd() * Len(sCharacters))+1, 1)
     Next


' copy file to server

set filesys=CreateObject("Scripting.FileSystemObject")
' im using a samba share here
filesys.CopyFile ObjFSO.FileName, "\\192.168.10.3\Internet\"& sRandom & ".zip"


' generate link


    
WScript.Echo "Your link has been copied to the clipboard"

' copy to clipboard

' this is the public domain address
strMessage = "downloads.mycompany.co.uk:10999/" & sRandom & ".zip"

' Declare an object for the word application '
Set objWord = CreateObject("Word.Application")

' Using the object '
With objWord
   .Visible = False     	' Don't show word '
   .Documents.Add       	' Create a document '
   .Selection.TypeText strMessage   ' Put text into it '
   .Selection.WholeStory    	' Select everything in the doc '
   .Selection.Copy  		' Copy contents to clipboard '
   .Quit False  		' Close Word, don't save ' 
End With

End If

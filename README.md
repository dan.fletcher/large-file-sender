this is a simple vbs scripts, was used on windows xp but will probably work on others.

for this to work you need a internally accessable network share, that same folder needs to be accessable under a public url, for security its generates a long random name. 

I used samba and lighttpd web server but you can use anything that meets the above criteria

The point being the user opens the network drive, clicks the link with prompts to select a file, in the backgroud it copies the file to the shared folder with a long random name, it creates a url to the file and puts into the paste, so you can just paste the url into an email and email someone. The recepient clicks the link and downlaoad the file via the webserver either http or ftp secure if you like. 

Thus enabling users to email effectively any size file you webserver/network share will support. Easy solution to get around the file size limitations of mail servers. Remember you may have a 100GB limit on your mail server but the receipent probably wont.
